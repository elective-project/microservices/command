# @elective-project/bell-node-module

## Description
It is a template of module.

## Clone
```shell
git clone https://gitlab.com/elective-project/bell-node-module.git
```

## Package installation
```shell
npm install
```

## Set up .env file
You must copy and paste the .env.example. After this step, you must fill the empty fields by its value. You can create different .env files:
- .env.local
- .env.development.local
- .env.test.local
- .env.production.local

```env
PORT = 5000

# Mongo
MONGO_CREDENTIAL =
MONGO_TIMEOUT = 5000

# MySQL
MYSQL_DRIVER = 
MYSQL_HOSTNAME =
MYSQL_DBNAME = 
MYSQL_USERNAME =
MYSQL_PASSWORD =
```

## Start the project
```shell
npm run dev
```
