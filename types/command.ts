
export interface ICommand {
    idUser: string
    idRestaurant: string
    idDelivery : string
    user: string
    restaurant: string
    userAddress: string
    restaurantAddress: string
    date: Date
    state: State
    articles: [{
        quantity: number
        article: string
        price: number
        options: [{
            option : string
            Choice : string
            price?: number
        }]
    }]
    priceArticles?: number
    priceDelivery: number
    price?: number

}

export interface ICommandHistory {
    idUser: string
    idRestaurant: string
    idDelivery : string
    user: string
    userAddress: string
    restaurantAddress: string
    date: Date
    state: State
    articles: [{
        quantity: number
        article: string
        price: number
        options: [{
            option : string
            Choice : string
            price?: number
        }]
    }]
    priceArticles?: number
    priceDelivery: number
    price?: number
    payment?: string

}

export enum State {
    RV = "restaurantValidation",
    DV = "deliveryValidation",
    RP = "restaurantPreparation",
    DW = "deliveryWaiting",
    OD = "outDelivery"
}



