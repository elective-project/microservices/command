import { body } from "express-validator";
/**
 * Validation for an user creation
 * 
 * @returns array of validation
 */
export const userCreateValidator = () => {
    return [
        body('surname', `Le prénom est invalide.`).isString().isLength({min: 3, max: 20}),
        body('name', `Le nom est invalide.`).isString().isLength({min: 3, max: 20}),
        body('password', `Le mot de passe est invalide.`).isStrongPassword({
            minLength: 8,
            minLowercase: 1,
            minUppercase: 1,
            minNumbers: 1,
            minSymbols: 1
        }).withMessage("Le mot de passe doit contenir au moins 8 caractères et au moins une majuscule, une minuscule, un chiffre et un symbole."),
    ];
}