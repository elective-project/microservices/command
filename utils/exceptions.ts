import { ApiException } from "../types/exception";

/**
 * Generic Exception class
 */
class Exception implements ApiException {
    constructor(readonly error: any, readonly status: number) {}
}
  
/**
 * Error 404
 */
export class NotFoundException extends Exception {
/**
 * Reference to the parent 
 */
    constructor(error: any) {
        super(error, 404);
    }
}
  
/**
 * Error 403
 */
export class ForbiddenException extends Exception {
    /**
     * Reference to the parent 
     */
    constructor(error: any) {
      super(error, 403);
    }
}

/**
 * Error 403
 */
 export class BadRequestException extends Exception {
    /**
     * Reference to the parent 
     */
    constructor(error: any) {
      super(error, 403);
    }
}

/**
 * Error 500
 */
 export class InternalErrorException extends Exception {
    /**
     * Reference to the parent 
     */
    constructor(error: any) {
      super(error, 500);
    }
}