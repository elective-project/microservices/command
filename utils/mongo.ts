import mongoose from "mongoose";
import { MongooseOptions } from "../config/mongoose.config";

mongoose.Promise = Promise;
let reconnectTimeout: any = process.env.MONGO_TIMEOUT;

/**
 * Connect to MongoDB server
 */
export function connectMongo() {
    mongoose.connect(process.env.MONGO_CREDENTIAL as string, MongooseOptions)
    .catch(() => {});
}

let db = mongoose.connection;

db.on('connecting', () => {
  console.info('Connecting to MongoDB...');
});

db.on('error', (error: string) => {
  console.error(`MongoDB connection error: ${error}`);
  mongoose.disconnect();
});

db.on('connected', () => {
  console.info('Connected to MongoDB!');
});

db.once('open', () => {
  console.info('MongoDB connection opened!');
});

db.on('reconnected', () => {
  console.info('MongoDB reconnected!');
});

db.on('disconnected', () => {
  console.error(`MongoDB disconnected! Reconnecting in ${reconnectTimeout / 1000}s...`);
  setTimeout(() => connectMongo(), reconnectTimeout);
});