import { Dialect, Sequelize } from 'sequelize';
import { readFileSync } from 'fs';
import dotenv from 'dotenv';

dotenv.config();

/**
 * Generic classs for Sequelize connection
 */
class SequelizeObject {
    private dbName: string;
    private dbUser: string;
    private dbHost: string | undefined;
    private dbDriver: string;
    private dbPassword: string | undefined;
    static instance: Sequelize | null = null;

    constructor () {
        this.dbName = process.env.MYSQL_DBNAME as string;
        this.dbUser = process.env.MYSQL_USERNAME as string;
        this.dbHost = process.env.MYSQL_HOSTNAME;
        this.dbDriver = process.env.MYSQL_DRIVER as Dialect;
        this.dbPassword = process.env.MYSQL_PASSWORD;
        this.authenticateMySql();
    }

    /**
     * Create a connection to the database if it doesn't exist
     * 
     * @returns sequelize connection
     */
    createConnection(): Sequelize {
        if(SequelizeObject.instance) {
            return SequelizeObject.instance;
        }
        SequelizeObject.instance = new Sequelize(this.dbName, this.dbUser, this.dbPassword, {
            host: this.dbHost,
            dialect: this.dbDriver as Dialect,
            dialectOptions: {
                ssl: {
                  cert: readFileSync('./certificates/DigiCertGlobalRootCA.crt.pem'), 
                }
            }
        });
        return SequelizeObject.instance;
    }
 
    /**
     * Test the authentication of mysql server
     */
    authenticateMySql() {
        this.createConnection()
        .authenticate()
        .then(res => console.log('Connection has been established successfully.'))
        .catch(error => console.log('Unable to connect to the database:', error));
    }
}

const sequelize = new SequelizeObject().createConnection();
Object.freeze(sequelize);

export {SequelizeObject, sequelize};
export default SequelizeObject;
