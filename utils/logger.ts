import * as winston from 'winston'
import {Logger} from "winston";
import dotenv from "dotenv";
dotenv.config()


const createLogger: Logger =  winston.createLogger({
    transports: [new winston.transports.Http({
        host: process.env.WINSTON_HOST,
        port: 443 ,
        path: process.env.WINSTON_PATH,
        headers: {
            service: process.env.WINSTON_SERVICE
        },
        ssl: true
    })],
});

const Logger = createLogger;

export {
    Logger
};