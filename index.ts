import cors from 'cors';
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { ExceptionsHandler } from './middlewares/exceptions.handler';
import { UnknownRoutesHandler } from './middlewares/unknowRoutes.handler'
import { connectMongo } from './utils/mongo';
import {CommandController} from "./resources/command/command.controller";
import {CommandHistoryController} from "./resources/command_history/command_history.controller";

dotenv.config();

/**
 * Global variables
 */
const app: Express = express();
const port = process.env.PORT || 5000;

/**
 * Express parsing in JSON
 *
 * @example app.post('/', (req) => req.body.prop)
 */
 app.use(express.json());

/**
 * Allows all domain names to access our APIS
 */
app.use(cors());

/**
 * Connection mongo
 * connectMongo();
 */
connectMongo();

app.use('/command/history',CommandHistoryController)
app.use('/command',CommandController )
app.get('/', (req, res) => res.send('🏠'))

/**
 * For all undefined routes
 */
app.all('*', UnknownRoutesHandler)

/**
  * Error manager
  */
app.use(ExceptionsHandler)

app.get('/', (req: Request, res: Response) => {
  res.send('Express + TypeScript Server'); 
});

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});