import { ConnectOptions } from "mongoose";

export let MongooseOptions: ConnectOptions = {
	autoIndex: true,
    socketTimeoutMS: process.env.MONGO_TIMEOUT as any,
    connectTimeoutMS: process.env.MONGO_TIMEOUT as any
};