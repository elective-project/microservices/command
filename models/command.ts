import {Schema, model} from "mongoose";
import {ICommand, ICommandHistory, State} from "../types/command";

const commandSchema = new Schema<ICommand>({
        idUser: {type: String, required: true},
        idRestaurant: {type: String, required: true},
        idDelivery: {type: String},
        user: {type: String},
        restaurant: {type: String},
        userAddress: {type: String, required: true},
        restaurantAddress: {type: String, required: true},
        date: {type: Date, required: true},
        state: {type: String, enum: State, required: true},
        articles: [{
            quantity: {type: Number, required: true},
            article: {type: String, required: true},
            price: {type: Number, required: true},
            options: [{
                option: {type: String, required: true},
                choice: {type: String, required: true},
                price: {type: Number}
            }]
        }],
        priceArticles: {type: Number},
        priceDelivery: {type: Number, required: true},
        price: {type: Number}
    },
    {collection: 'command'});

const commandHistorySchema = new Schema<ICommandHistory>({
        idUser: {type: String, required: true},
        idRestaurant: {type: String, required: true},
        idDelivery: {type: String, required: true},
        user: {type: String},
        userAddress: {type: String, required: true},
        restaurantAddress: {type: String, required: true},
        date: {type: Date, required: true},
        state: {type: String, enum: State},
        articles: [{
            quantity: {type: Number, required: true},
            article: {type: String, required: true},
            price: {type: Number, required: true},
            options: [{
                option: {type: String, required: true},
                choice: {type: String, required: true},
                price: {type: Number}
            }]
        }],
        priceArticles: {type: Number},
        priceDelivery: {type: Number, required: true},
        price: {type: Number}
    },
    {collection: 'command_history'});

export const commandMongoose = model<ICommand>('command', commandSchema)
export const commandHistoryMongoose = model<ICommandHistory>('command_history', commandHistorySchema)