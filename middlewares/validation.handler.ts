import { NextFunction, Request, Response } from 'express';
import { validationResult } from "express-validator"
import { BadRequestException } from '../utils/exceptions';

/**
 * General validator manager
 *
 * @param err - Express error
 * @param req - The initial request
 * @param res - The response object
 * @param next - Allows to go to the next middleware if existing
 *
 */
export const validate = (req:  Request, res: Response, next: NextFunction) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
      return next();
    }
    const extractedErrors: any = [];
    errors.array({ onlyFirstError: true }).map(err => extractedErrors.push({ [err.param]: err.msg }));

    throw new BadRequestException(extractedErrors); 
}