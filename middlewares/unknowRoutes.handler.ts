import { NotFoundException } from '../utils/exceptions';
import { NextFunction, Request, Response } from 'express';

/**
 * Return an error for undefined routes
 */
export const UnknownRoutesHandler = (req: Request, res: Response, next: NextFunction) => {
  throw new NotFoundException('The resource does not exist.');
}