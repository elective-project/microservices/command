import { NextFunction, Request, Response } from 'express';
import { InternalErrorException } from '../utils/exceptions';

/**
 * Global error management middleware
 *
 * @param err - Express error
 * @param req - The initial request
 * @param res - The response object
 * @param next - Allows to go to the next middleware if existing
 *
 * @see https://expressjs.com/en/guide/error-handling.html
 */
export const ExceptionsHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  
  if (res.headersSent) {
    return next(err);
  }

  if (err.status && err.error) {
    return res
      .status(err.status)
      .json({ error: err.error });
  }

  throw new InternalErrorException("Internal error");
}