import {Router} from 'express'
import {CommandService} from "./command.service";
import {Logger} from '../../utils/logger'

const CommandController = Router()

const service = new CommandService()

CommandController.get('/:id', (req, res) => {
    service.getOne(req.params.id).then((command) => {
        res.status(201).json(command)
    }).catch((err) => {
        Logger.error('Error getOne message: ' + err.message)
        res.status(404).send(err.message);
    });
})

CommandController.post('/', (req, res) => {
    service.checkIfExist(req.body.idUser).then((command) => {
        if (command !== null) {
            res.status(404).send('command already exist for this user')
            return
        }
        service.postOne(req.body).then((data: any) => {
            Logger.info(`post ${data._id} in command `)
            res.status(201).send('command create')
        }).catch((err) => {
            Logger.error('Error postOne message: ' + err.message)
            res.status(404).send(err.message);
        });

    })
})

CommandController.put('/', (req, res) => {
    service.updateOne(req.body.idUser, req.body.state, req.body.idDelivery).then((command: any) => {
        Logger.info(`put ${command._id} in command `)
        res.status(201).send('command update')
    }).catch((err) => {
        Logger.error('Error updateOne message:' + err.message)
        res.status(404).send(err.message);
    });
})

CommandController.delete('/:id', (req, res) => {
    service.deleteOne(req.params.id).then((command) => {
        Logger.info(`delete ${req.params.id} in command `)
        res.status(201).send('command delete')
    }).catch((err) => {
        Logger.error('Error deleteOne message:' + err.message)
        res.status(404).send(err.message);
    });
})

export {CommandController}