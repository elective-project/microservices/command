import {commandMongoose} from "../../models/command";
import {ICommand, State} from "../../types/command";
import axios from "axios";

export class CommandService {

    async getOne(id: string): Promise<ICommand | null> {
        return commandMongoose.findOne({idUser: id});
    }

    async postOne(data: ICommand): Promise<ICommand> {
        const dataModel = new commandMongoose(data);
        const name = await this.getUserName(data.idUser)
        dataModel.user = name
        return dataModel.save();
    }

    async getUserName(id: string) {
        console.log(id)
        return axios.get('https://rep-eat-user-france.azurewebsites.net/users/' + id, {}).then(response => {
            console.log(response.data)
            return response.data.name + ' ' + response.data.surname
        })
    }

    async updateOne(id: number, state: State, idDelivery?: string): Promise<ICommand> {
        const filter = {idUser: id}
        return commandMongoose.findOne(filter, {new: true}).then((command) => {
            if (!command) {
                throw new Error('command does not exist');
            }
            command.state = state;
            if (idDelivery) {
                command.idDelivery = idDelivery
            }
            return command.save();
        })
    }

    async deleteOne(id: string) {
        return commandMongoose.deleteOne({idUser: id});
    }

    async checkIfExist(idUser: number): Promise<ICommand | null> {
        return commandMongoose.findOne({idUser: idUser});
    }

}