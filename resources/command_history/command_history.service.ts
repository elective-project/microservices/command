import {commandHistoryMongoose} from "../../models/command";
import {ICommandHistory, State} from "../../types/command";

export class CommandHistoryService {

    async get(filter: any): Promise<ICommandHistory[] | null> {
        return commandHistoryMongoose.find(filter);
    }

    async postOne(data: ICommandHistory): Promise<ICommandHistory> {
        const dataModel = new commandHistoryMongoose(data);
        return dataModel.save();
    }

}