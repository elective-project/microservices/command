import {Router} from 'express'
import {CommandHistoryService} from "./command_history.service";
import {Logger} from '../../utils/logger'

const CommandHistoryController = Router()

const service = new CommandHistoryService()

CommandHistoryController.post('/list', (req, res) => {
    service.get(req.body).then((command) => {
        res.status(201).json(command)
    }).catch((err) => {
        Logger.error('Error get message: ' + err.message)
        res.status(404).send(err.message);
    });
});

CommandHistoryController.post('/', (req, res) => {
    service.postOne(req.body).then((data: any) => {
        Logger.info(`post ${data._id} in command history`)
        res.status(201).send('command history create')
    }).catch((err) => {
        Logger.error('Error postOne message: ' + err.message)
        res.status(404).send(err.message);
    });
})

export {CommandHistoryController}